/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentjava;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;

import javafx.application.Platform;


public class TaskReadThread implements Runnable {
    Socket socket;
    ClientJavaFX client;
    DataInputStream input;

    public TaskReadThread(Socket socket, ClientJavaFX client) {
        this.socket = socket;
        this.client = client;
    }

    @Override
    public void run() {
        while (true) {
            try {
                //data input
                input = new DataInputStream(socket.getInputStream());

                //recup l'input a envoyer
                String message = input.readUTF();
                int nbUsers = input.read();

                Platform.runLater(() -> {
                    //ecris le message dans la zone de text
                    client.txtAreaDisplay.appendText(message + "\n");
                    client.labelUsersConnectes.setText(String.valueOf(nbUsers));
                });
            } catch (IOException ex) {
                System.out.println("Error reading from server: " + ex.getMessage());
                ex.printStackTrace();
                break;
            }
        }
    }
    public void setNbUsers(String str){
        client.labelUsersConnectes.setText(str);
    }

  }


