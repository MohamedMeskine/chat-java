
package commentjava;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javafx.application.Platform;

public  class TaskClientConnection implements Runnable {

    Socket socket;
    ServerJavaFX server;
    DataInputStream input;
    DataOutputStream output;

    public TaskClientConnection(Socket socket, ServerJavaFX server) {
        this.socket = socket;
        this.server = server;

    }

    @Override
    public void run() {

        try {
            // input et output
            input = new DataInputStream(
                    socket.getInputStream());
            output = new DataOutputStream(
                    socket.getOutputStream());
            //this.sendMessage("Un nouvel utilisateur à rejoint !");
            Platform.runLater(() -> {
                server.broadcast("Un nouvel utilisateur à rejoint !",server.connectionList.size());
                server.labelUsersConnectes.setText(String.valueOf(server.connectionList.size()));

            });

            while (true) {
                // recois les messages
                String message = input.readUTF();

                //evoie en Broadcast
                server.broadcast(message, server.connectionList.size());

                Platform.runLater(() -> {
                    server.textZoneMessage.appendText(message + "\n");
                });
            }



        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }

    }

    //message au client
    public void sendMessage(String message, int nbUsers) {
        try {
            output.writeUTF(message);
            output.write(nbUsers);
            output.flush();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }



}
