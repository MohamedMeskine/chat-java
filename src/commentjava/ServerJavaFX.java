/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commentjava;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ServerJavaFX extends Application {
    public TextArea textZoneMessage;
    public List<TaskClientConnection> connectionList = new ArrayList<TaskClientConnection>();
    Label labelUsers;
    Label labelUsersConnectes;

    @Override
    public void start(Stage primaryStage) {
        textZoneMessage = new TextArea();
        textZoneMessage = new TextArea();
        textZoneMessage.setEditable(false);

        VBox vBox = new VBox();

        labelUsers = new Label("Nb d'utilisateurs connectés :");
        labelUsersConnectes = new Label("0");
        HBox hBoxUsers = new HBox(labelUsers, labelUsersConnectes);



        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(textZoneMessage);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        vBox.getChildren().addAll( hBoxUsers, scrollPane);
        vBox.setVgrow(scrollPane, Priority.ALWAYS);

        Scene scene = new Scene(vBox, 450, 500);
        primaryStage.setTitle("COMMENT JAVA?! lancé !!");
        System.out.println("oké lets go");
        primaryStage.setScene(scene);
        primaryStage.show();

        //creation du thread
        new Thread(() -> {
            try {
                //server socket
                ServerSocket serverSocket = new ServerSocket(ConnectionUtil.port);


                Platform.runLater(()
                        -> textZoneMessage.appendText("Serveur lancé à " + new Date() + '\n'));


                while (true) {
                    // ecoute en permanance les requetes de connexion
                    Socket socket = serverSocket.accept();
                    TaskClientConnection connection = new TaskClientConnection(socket, this);
                    connectionList.add(connection);

                    //create a new thread
                    Thread thread = new Thread(connection);
                    thread.start();

                }
            } catch (IOException ex) {
                textZoneMessage.appendText(ex.toString() + '\n');
            }
        }).start();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public int getNbUsers(){
        return connectionList.size();
    }
    //envoie a tout le monde les messages recu
    public void broadcast(String message, int nbUsers) {
        for (TaskClientConnection clientConnection : this.connectionList) {
            clientConnection.sendMessage(message, nbUsers);
        }
    }


}
