
package commentjava;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ClientJavaFX extends Application {
    TextField pseudo;
    TextField input;
    ScrollPane scrollPane;
    Label labelUsers;
    Label labelUsersConnectes;
    public TextArea txtAreaDisplay;

    DataOutputStream output = null;

    @Override
    public void start(Stage primaryStage) {
        BackgroundFill background_fill = new BackgroundFill(Color.GRAY,
                CornerRadii.EMPTY, Insets.EMPTY);

        // creation du Background
        Background background = new Background(background_fill);
        //pane pour forcer le scroll et VBox
        VBox vBox = new VBox();

        scrollPane = new ScrollPane();
        //pane pour les messages
        HBox hBox = new HBox();

        txtAreaDisplay = new TextArea();
        txtAreaDisplay.setBackground(background);
        txtAreaDisplay.setEditable(false);

        scrollPane.setContent(txtAreaDisplay);
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        //textfield et button
        pseudo = new TextField();
        pseudo.setPromptText("Pseudo");
        pseudo.setTooltip(new Tooltip("Entrez votre pseudo. "));

        input = new TextField();
        input.setPromptText("Nouveau message");
        input.setTooltip(new Tooltip("Ecrivez votre message: "));
        input.setBackground(background);
        input.setOnKeyPressed(new EventHandler<KeyEvent>() //code qui devant permettre le keyup.enter
        {
            @Override
            public void handle(KeyEvent ke)
            {
                if (ke.getCode().equals(KeyCode.ENTER))
                {
                    new ButtonListener();
                }
            }
        });
        Button btnSend = new Button("Envoyer");
        btnSend.setOnAction(new ButtonListener());
        hBox.getChildren().addAll(pseudo, input, btnSend);
        hBox.setHgrow(input, Priority.ALWAYS);  //fenetre sizable

        //user co


        labelUsers = new Label("Nb d'utilisateurs connectés :");
        labelUsersConnectes = new Label();
        HBox hBoxUsers = new HBox(labelUsers, labelUsersConnectes);

        // set background

        vBox.getChildren().addAll(hBoxUsers, scrollPane, hBox, txtAreaDisplay);
        vBox.setVgrow(scrollPane, Priority.ALWAYS);

        Scene scene = new Scene(vBox, 450, 500);
        primaryStage.setTitle("CommentJAVA ?!");

        primaryStage.getIcons().add(new Image("/commentjava/logo.png"));
        primaryStage.setScene(scene);
        primaryStage.show();

        try {

            String username = pseudo.getText().trim();
            // Creation du socket
            Socket socket = new Socket(ConnectionUtil.host, ConnectionUtil.port);

            //Connection reussi !
            System.out.println("OHYEAH");
            txtAreaDisplay.appendText("Connecté.e. \n");


            output = new DataOutputStream(socket.getOutputStream());
            output.writeUTF(username);
            //Thread pour lire les messages quand ils arrivent
            TaskReadThread task = new TaskReadThread(socket, this);
            Thread thread = new Thread(task);
            thread.start();


        } catch (IOException ex) {

            txtAreaDisplay.appendText(ex.toString() + '\n');
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Action lorsque le bouton "envoyer" est actionné
     */
    private class ButtonListener implements EventHandler<ActionEvent> {

        @Override
        public void handle(ActionEvent e) {
            try {
                //get username et message
                String username = pseudo.getText().trim();
                String message = input.getText().trim();

                //si username vide = 'Inconnu'
                if (username.length() == 0) {
                    username = "Inconnu";
                }
                //on envoie pas de message vide
                if (message.length() == 0) {
                    return;
                }

                //envoyé !
                System.out.println("message envoyé!");
                output.writeUTF("" + username + ": " + message + "");
                dbConn.insertData(username,message);
                output.flush();

                input.clear();
            } catch (IOException ex) {
                System.err.println(ex);
            }

        }
    }

}